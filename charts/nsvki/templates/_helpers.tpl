{{/*
Expand the name of the chart.
*/}}
{{- define "nsvki.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "nsvki.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" $name .Release.Name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "nsvki.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "nsvki.labels" -}}
helm.sh/chart: {{ include "nsvki.chart" . }}
{{ include "nsvki.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "nsvki.selectorLabels" -}}
app.kubernetes.io/name: {{ include "nsvki.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{- define "nsvki.databaseDsn" -}}
postgresql+psycopg2://{{ .Values.postgresql.auth.username }}:{{ .Values.postgresql.auth.password }}@{{ (include "nsvki.fullname" .) }}-postgresql/{{ .Values.postgresql.auth.database }}
{{- end -}}

{{- define "nsvki.brokerDsn" -}}
redis://:{{ .Values.redis.auth.password }}@{{ (include "nsvki.fullname" .) }}-redis-master
{{- end -}}

{{/*
Create the environment variables used by the Python app
*/}}
{{- define "nsvki.appEnvironment" }}
  - name: SQLALCHEMY_DATABASE_URI
    value: {{ include "nsvki.databaseDsn" . }}
  - name: POSTGRES_HOST
    value: {{ (include "nsvki.fullname" .) }}-postgresql
  - name: POSTGRES_USER
    value: {{ .Values.postgresql.auth.username }}
  - name: POSTGRES_PASS
    value: {{ .Values.postgresql.auth.password }}
  - name: POSTGRES_DB
    value: {{ .Values.postgresql.auth.database }}
  - name: BROKER_URL
    value: {{ include "nsvki.brokerDsn" . }}
  - name: S3_BUCKET
    value: {{ .Values.nsvki.s3.bucket  }}
  - name: AWS_ACCESS_KEY_ID
    value: {{ .Values.nsvki.s3.aws_access_key_id  }}
  - name: AWS_SECRET_ACCESS_KEY
    value: {{ .Values.nsvki.s3.aws_secret_access_key  }}
{{ if .Values.nsvki.googleApiKeyExistingSecret }}
  - name: GOOGLE_API_KEY_FILE
    value: "/secrets/google/api-key.json"
{{ end }}
{{- end }}


{{/*
App volumes and volumeMounts for secret files (api keys).
*/}}
{{- define "nsvki.volumeMounts" -}}
{{- if .Values.nsvki.googleApiKeyExistingSecret }}
  - name: google-api-key-file
    mountPath: /secrets/google/
    readOnly: true
{{- end }}
{{- if .Values.nsvki.samlCertsExistingSecret }}
  - name: saml-certs
    mountPath: /app/saml/{{ .Values.nsvki.samlEnvironment }}/certs/
    readOnly: true
{{- end }}
{{- end }}

{{- define "nsvki.volumes" -}}
{{- if .Values.nsvki.googleApiKeyExistingSecret }}
  - name: google-api-key-file
    secret:
      secretName: {{ .Values.nsvki.googleApiKeyExistingSecret }}
      items:
        - key: google-api-key
          path: "api-key.json"
{{- end }}
{{- if .Values.nsvki.samlCertsExistingSecret }}
  - name: saml-certs
    secret:
      secretName: {{ .Values.nsvki.samlCertsExistingSecret }}
{{- end }}
{{- end }}
